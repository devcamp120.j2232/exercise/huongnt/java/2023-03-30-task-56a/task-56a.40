package com.devcamp.circlerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CircleRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircleRestapiApplication.class, args);
	}

}

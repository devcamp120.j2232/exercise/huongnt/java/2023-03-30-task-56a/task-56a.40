package com.devcamp.circlerestapi;

public class Circle {
    double radius = 1.0;

    //khởi tạo phương thức
    public Circle() {
    }
    
    public Circle(double radius) {
        this.radius = radius;
    }

    //getter & setter
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    //các phương thức khác
    public double getArea() {
        return Math.PI * Math.pow(this.radius,2);
    }

    public double getPerimeter() {
        return 2 * Math.PI * this.radius;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }
    
    
}

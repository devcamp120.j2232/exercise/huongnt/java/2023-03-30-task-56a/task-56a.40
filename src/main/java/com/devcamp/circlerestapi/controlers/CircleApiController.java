package com.devcamp.circlerestapi.controlers;
import java.util.ArrayList;
import com.devcamp.circlerestapi.Circle;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.devcamp.circlerestapi.Circle;
@RestController
@RequestMapping("/")
@CrossOrigin
public class CircleApiController {
    @GetMapping("/circle-area")
    public double circleArea(){

        Circle circle1 = new Circle(2);

        //in ra console
        System.out.println(circle1.toString());
                
    
        return circle1.getArea();

    }
    
}
